﻿using System;
using RPGCharacterBuilder.Characters;
using RPGCharacterBuilder.Items;

namespace RPGCharacterBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            // use "level" parameter as base, and tell the user to change the value in order to level up, maybe set a levelUp Function or something fun to change the level

            Mage Bob = new Mage("Bob",10);
            //Use To string method described in the assignment
            Console.WriteLine("Strength " + Bob.strength);
            Console.WriteLine("Dexterity " + Bob.dexterity);
            Console.WriteLine("Intelligence " + Bob.intelligence);





        }
    }
}
