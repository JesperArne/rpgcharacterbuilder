﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterBuilder.Characters
{
    public class Mage : Character
    {
        public string name { get; set; }
        public int level { get; set; }
        public string armor { get; set; }



        public Mage(string name, int level) : base(name, level)
        {
            this.name = name;
            this.level = level;
            this.armor = armor;
            CharacterType = "Mage";
            //CharacterType = this.MageType;

            birth();
            baseAttributes();
            totalAttributes(level);
        
        }



        public void birth() 
        {
            Console.WriteLine($"i am a {CharacterType} and my name is {name}");
            
        }
        override public void baseAttributes() //Remember to increase damage by 1 % based on mage logic
        {
            baseStrength = 1;
            baseDexterity = 1;
            baseintelligence = 8;
        }

        //override public void totalAttributes(int level)
        //{
        //}

    }
}
