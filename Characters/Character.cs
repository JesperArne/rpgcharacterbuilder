﻿using RPGCharacterBuilder.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterBuilder.Characters
{
    public abstract class Character
    {
        private string name;
        private int level = 1;
        private string characterType;

        public Character(string Name, int Level)
        {
            string characterName = Name;
            int charectorLevel = Level;
            characterType = CharacterType;
            Console.WriteLine("A charecter is born");
        }


        public string Name { get => name; set => name = value; } // accses private stuff in class contructor
        public int Level { get => level; set => level = value; }

        public string CharacterType { get => characterType; set => characterType = value; }

        public int strength { get; set; }
        public int dexterity { get; set; }
        public int intelligence { get; set; }
        public int baseStrength { get; set; }
        public int baseDexterity { get; set; }
        public int baseintelligence { get; set; }

        public Equipment Equipment { get; set; }


        public abstract void baseAttributes();
        public virtual void totalAttributes(int level)//might also apply to item bonuses later 
        {
            for (int i = 0; i < level; i++)
            {
                strength = strength + baseStrength;
                dexterity = dexterity + baseDexterity;
                intelligence = intelligence + baseintelligence;
            }
        }
        public bool Equipable(EquipmentSlots slots, Item item) 
        {
            Equipment.Slots.Add(slots,item);

            return true;
        }

    }
}
