﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterBuilder.Characters
{
    public class Ranger : Character
    {
        public string name { get; set; }
        public int level { get; set; }
        


        public Ranger(string name, int level) : base(name, level)
        {
            this.name = name;
            this.level = level;

            birth();
            baseAttributes();
            totalAttributes(level);
        
        }



        public void birth() 
        {
            Console.WriteLine("i am a ranger and my name is " + name);
        }
        override public void baseAttributes()
        {
            baseStrength = 1;
            baseDexterity = 5;
            baseintelligence = 1;
        }

        //override public void totalAttributes(int level)
        //{
        //}

    }
}
