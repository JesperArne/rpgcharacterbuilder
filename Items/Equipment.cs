﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterBuilder.Items
{
    public class Equipment
    {
        public Dictionary<EquipmentSlots, Item> Slots { get; set; }
    }
}
