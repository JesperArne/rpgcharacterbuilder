﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterBuilder.Items
{
    public abstract class Item
    {
        public string Name { get; set; }
        public string SlotPlacement { get; set; }
        public int RequiredLevel { get; set; }
        

        //Create a function that checks the characters level/Type in order to throwexeptions, if the character is too low level or wrong type 
        //      - This could be a bool function that allows for a calculate dps function to run

        //Create a function that calculates the DPS described on page 5 

    }
}
